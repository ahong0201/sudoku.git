package com.csdn.enums4;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ShapeTypeEnum {

    DEFAULT("", new Default()) {
        @Override
        public Shape getShapeImpl() {
            return new Default();
        }
    },
    CIRCLE("CIRCLE", new Circle()) {
        @Override
        public Shape getShapeImpl() {
            return new Circle();
        }
    },
    RECTANGLE("RECTANGLE", new Rectangle()) {
        @Override
        public Shape getShapeImpl() {
            return new Rectangle();
        }
    },
    SQUARE("SQUARE", new Square()) {
        @Override
        public Shape getShapeImpl() {
            return new Square();
        }
    },
    ;

    /**
     * 功能描述: 获取实现 <br/>
     *
     * @return "com.csdn.enums4.Shape"
     */
    public abstract Shape getShapeImpl();

    /**
     * 功能描述: 单例 <br/>
     *
     * @param type 类型
     * @return "com.csdn.enums4.Shape"
     */
    public static Shape getSingletonShape(String type) {
        return indexOf(type).getShape();
    }

    /**
     * 功能描述: 非单例 <br/>
     *
     * @param type 类型
     * @return "com.csdn.enums4.Shape"
     */
    public static Shape getNonSingletonShape(String type) {
        return indexOf(type).getShapeImpl();
    }

    private final static Map<String, ShapeTypeEnum> map = Stream.of(values()).collect(Collectors.toMap(ShapeTypeEnum::getType, e -> e));

    public static ShapeTypeEnum indexOf(String type) {
        // 避免出现空指针
        return Optional.ofNullable(map.get(type)).orElse(DEFAULT);
    }

    private final String type;
    private final Shape shape;

    ShapeTypeEnum(String type, Shape shape) {
        this.type = type;
        this.shape = shape;
    }

    public String getType() {
        return type;
    }

    public Shape getShape() {
        return shape;
    }

}
