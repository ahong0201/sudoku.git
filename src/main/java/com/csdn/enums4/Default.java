package com.csdn.enums4;

public class Default implements Shape {
 
   @Override
   public void draw() {
      System.out.println("Inside Default::draw() method." + this);
   }
}