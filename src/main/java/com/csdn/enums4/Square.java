package com.csdn.enums4;

public class Square implements Shape {
 
   @Override
   public void draw() {
      System.out.println("Inside Square::draw() method." + this);
   }
}