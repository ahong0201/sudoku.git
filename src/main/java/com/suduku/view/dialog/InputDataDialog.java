package com.suduku.view.dialog;

import com.suduku.view.data.SudoData;
import com.suduku.view.constant.ViewConstant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 功能描述: 录入数据弹窗 <br/>
 */
public class InputDataDialog extends JDialog implements ActionListener {

    private final JTextField text = new JTextField();

    public InputDataDialog() {
        this.setTitle("录入初始数据");
        this.setSize(800, 80);
        //
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        text.setText("");
        this.add(text, BorderLayout.CENTER);
        JButton button = new JButton("确定");
        button.addActionListener(this);
        this.add(button, BorderLayout.EAST);
        // 父窗口不可操作
        this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        // 显示窗口
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        // 校验 str 是否满足数独
        try {
            SudoData.initData(this.text.getText());
            ViewConstant.hintBut.setEnabled(true);
            ViewConstant.markAllBut.setEnabled(true);
            ViewConstant.clearMarkBut.setEnabled(true);
            ViewConstant.calcLabel.setText("                    ");
            this.dispose();
        } catch (RuntimeException e) {
            new MessageDialog(e.getMessage());
        }
    }

}
