package com.suduku.view.dialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MessageDialog extends JDialog implements ActionListener {

    public MessageDialog(String message) {
        this.setTitle("提示");
        this.setSize(200, 100);
        // 居中
        this.setLocationRelativeTo(null);
        // 不允许窗口最大化
        this.setResizable(false);
        // 添加消息标签
        this.add(new JLabel(message), BorderLayout.CENTER);

        JPanel panel = new JPanel();
        JButton button = new JButton("确定");
        button.addActionListener(this);
        panel.add(button);
        this.add(panel, BorderLayout.SOUTH);
        // 父窗口不可操作
        this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        // 显示窗口
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.dispose();
    }

}
