package com.suduku.view;

import com.suduku.view.constant.ViewConstant;
import com.suduku.view.listener.adapter.SudoFrameAdapter;
import com.suduku.view.panel.CandidatePanel;
import com.suduku.view.panel.OperatePanel;

import javax.swing.*;
import java.awt.*;

/**
 * 功能描述: 数独主界面 <br/>
 */
public class SudoFrame extends JFrame {

    public static void main(String[] args) {
        // 初始化界面
        new SudoFrame();
    }

    public SudoFrame() {
        this.setTitle("数独游戏");
        init();
        this.setVisible(true);
        this.addWindowListener(new SudoFrameAdapter());
    }

    /**
     * 功能描述: 初始化 <br/>
     *
     */
    private void init() {
        this.setBounds(400, 0, 600, 550);
        // 不允许窗口最大化
        this.setResizable(false);
        // 不执行任何操作;要求程序在已注册的 WindowListener 对象的 windowClosing 方法中处理该操作
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // 居中
        this.setLocationRelativeTo(null);
        // 主体数独
        this.add(ViewConstant.sudoPanel, BorderLayout.CENTER);
        // 操作区
        this.add(new OperatePanel(), BorderLayout.EAST);
        // 候选数字
        this.add(new CandidatePanel(), BorderLayout.SOUTH);
        this.setVisible(true);
    }

}
