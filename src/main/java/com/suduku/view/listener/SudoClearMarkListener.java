package com.suduku.view.listener;

import com.suduku.view.constant.ViewConstant;
import com.suduku.view.data.SudoData;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 功能描述: 清除标记按钮监听 <br/>
 */
public class SudoClearMarkListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        SudoData.CX = -1;
        SudoData.CY = -1;
        // 遍历全部数据
        SudoData.sudo.getBoxList().forEach(box -> {
            // 如果没有数字
            if(box.isBlank()) {
                // 添加标记
                SudoData.markMap.remove(box.getX() + "" + box.getY());
                // 移除
                ViewConstant.sudoPanel.remove(box.getI());
                // 添加标记面板
                ViewConstant.sudoPanel.add(ViewConstant.numberFields[box.getX()][box.getY()], box.getI());
            } else {
                ViewConstant.numberFields[box.getX()][box.getY()].setBackground(Color.WHITE);
            }
        });
        // 刷新
        ViewConstant.sudoPanel.repaint();
        ViewConstant.sudoPanel.validate();
    }

}
