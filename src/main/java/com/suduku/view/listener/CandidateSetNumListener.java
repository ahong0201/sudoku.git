package com.suduku.view.listener;

import com.suduku.entity.Box;
import com.suduku.view.constant.ViewConstant;
import com.suduku.view.data.SudoData;
import com.suduku.view.panel.SudoPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CandidateSetNumListener implements ActionListener {
    /**
     * 功能描述: 按钮监听 <br/>
     *
     * @param e 事件
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String text = e.getActionCommand();
        if(text.equals("<")) {
            if(SudoData.count > 0) {
                // 回退
                SudoData.restore();
                SudoPanel.refreshSudoPanel();
            }
            ViewConstant.calcLabel.setText("                    ");
        } else if(text.equals("×")) {
            // 操作前备份
            SudoData.backup();
            ViewConstant.numberFields[SudoData.CX][SudoData.CY].setText("");
            Box box = SudoData.getByIndex(SudoData.CX * 9 + SudoData.CY);
            SudoData.sudo.removeAndResetCList(box);
            ViewConstant.enableButton(box.getCList(), false);
        } else {
            ViewConstant.setNum(text);
        }
    }

}
