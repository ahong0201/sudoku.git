package com.suduku.view.listener;

import com.suduku.entity.Box;
import com.suduku.view.data.SudoData;
import com.suduku.view.constant.ViewConstant;

import javax.swing.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class SudoTextFocusListener implements FocusListener {

    /**
     * 功能描述: 获取焦点 <br/>
     *
     * @param e 焦点事件
     */
    @Override
    public void focusGained(FocusEvent e) {
        JTextField jtf = (JTextField) e.getSource();
        String name = jtf.getName();
        Box box;
        if(name.indexOf(",") > 0) {
            String[] sp = name.split(",");
            box = SudoData.getByIndex(Integer.parseInt(sp[0]) * 9 + Integer.parseInt(sp[1]));
        } else {
            box = SudoData.getByIndex(name);
        }
        // 避免初始化时的自动获取焦点问题
        if(box.getI() > -1) {
            // 设置当前选中的坐标
            SudoData.CX = box.getX();
            SudoData.CY = box.getY();
            ViewConstant.focusGained(box);
        }
    }

    /**
     * 功能描述: 失去焦点 <br/>
     *
     * @param e 焦点事件
     */
    @Override
    public void focusLost(FocusEvent e) {
        // 清楚选中的坐标

    }

}
