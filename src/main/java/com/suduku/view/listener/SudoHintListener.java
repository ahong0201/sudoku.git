package com.suduku.view.listener;

import com.suduku.calc.enums.CalcEnum;
import com.suduku.calc.register.SudoCalcRegister;
import com.suduku.entity.Box;
import com.suduku.view.constant.ViewConstant;
import com.suduku.view.data.SudoData;
import com.suduku.view.dialog.MessageDialog;
import com.suduku.view.panel.SudoPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 功能描述: 提示按钮监听 <br/>
 */
public class SudoHintListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        // 调用全部标记
        ViewConstant.markAllBut.doClick();
        // 操作前备份
        SudoData.backup();
        // 循环算法
        boolean flag = false;
        for(CalcEnum ce : CalcEnum.values()) {
            Box box = SudoCalcRegister.get(ce).calcSolve();
            if(box != null) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            // 如果无效，则还原
            SudoData.restore();
            SudoPanel.refreshSudoPanel();
            new MessageDialog("没有找到解法");
        }

    }

}
