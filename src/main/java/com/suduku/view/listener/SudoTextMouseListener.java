package com.suduku.view.listener;

import com.suduku.entity.Box;
import com.suduku.view.data.SudoData;
import com.suduku.view.constant.ViewConstant;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class SudoTextMouseListener implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {
        if(SudoData.sudo == null) {
            return;
        }
        if(e.getButton() == MouseEvent.BUTTON1) {
//            System.out.println("鼠标左键");
        }
        if(e.getButton() == MouseEvent.BUTTON3) {
            System.out.println("鼠标右键");
            if(e.getSource() instanceof JTextField) {
                JTextField jtf = (JTextField) e.getSource();
                System.out.println("鼠标单击事件：" + jtf.getName());
                Box box = SudoData.getByIndex(jtf.getName());
                if(!box.isInitNum()) {
                    SudoData.CX = box.getX();
                    SudoData.CY = box.getY();
                    // 标记区域
                    ViewConstant.sudoPanel.remove(box.getI());
                    JPanel panel = ViewConstant.numberPanels[box.getX()][box.getY()];
                    ViewConstant.sudoPanel.add(panel, box.getI());
                    ViewConstant.sudoPanel.validate();
                }
            } else if(e.getSource() instanceof JLabel) {
                JLabel label = (JLabel) e.getSource();
                String name = label.getName();
                String[] sp = name.split(",");
                System.out.println("选中了label：" + name);
                System.out.println("x:" + sp[0]);
                System.out.println("y:" + sp[1]);
                System.out.println("n:" + sp[2]);
            } else {
                System.out.println("222222222222222");
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //System.out.println("鼠标按下");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //System.out.println("鼠标松开");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //System.out.println("鼠标进入组件");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //System.out.println("鼠标退出组件");
    }

}
