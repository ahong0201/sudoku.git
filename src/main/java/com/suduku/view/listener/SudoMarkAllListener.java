package com.suduku.view.listener;

import com.suduku.view.constant.ViewConstant;
import com.suduku.view.data.SudoData;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 功能描述: 全部标记按钮监听 <br/>
 */
public class SudoMarkAllListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        SudoData.CX = -1;
        SudoData.CY = -1;
        // 遍历全部数据
        SudoData.sudo.getBoxList().forEach(box -> {
            // 如果没有数字
            if(box.isBlank()) {
                // 添加标记
                SudoData.markMap.put(box.getX() + "" + box.getY(), true);
                // 移除
                ViewConstant.sudoPanel.remove(box.getI());
                // 添加标记面板
                ViewConstant.sudoPanel.add(ViewConstant.numberPanels[box.getX()][box.getY()], box.getI());
                // 添加标记数字
                box.getCList().forEach(c -> ViewConstant.numberLabels[box.getX()][box.getY()][c - 1].setText(c + ""));
                for(int n = 0; n < 9; n++) {
                    ViewConstant.numberLabels[box.getX()][box.getY()][n].setBackground(Color.WHITE);
                }
            } else {
                ViewConstant.numberFields[box.getX()][box.getY()].setBackground(Color.WHITE);
            }
        });
        // 刷新
        ViewConstant.sudoPanel.validate();
    }

}
