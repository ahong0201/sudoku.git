package com.suduku.view.panel;

import com.suduku.view.data.SudoData;
import com.suduku.view.constant.ViewConstant;
import com.suduku.view.listener.SudoTextFocusListener;
import com.suduku.view.listener.SudoTextMouseListener;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * 功能描述: 数独主体画板 <br/>
 */
public class SudoPanel extends JPanel {

    public SudoPanel() {
        this.setBackground(ViewConstant.lightGray);
        this.setLayout(new GridLayout(9, 9, 0, 0));
        // 加边框
        this.setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.GRAY));
        initSudoPanel();
    }

    /**
     * 功能描述: 初始化数独区域面板 <br/>
     *
     */
    public void initSudoPanel() {
        // 添加 每个小的单元格，都添加9个
        Border centerBorder = BorderFactory.createMatteBorder(1, 1, 1, 1, Color.GRAY);
        Border rightAndBottomBorder = BorderFactory.createMatteBorder(1, 1, 4, 4, Color.GRAY);
        Border bottomBorder = BorderFactory.createMatteBorder(1, 1, 4, 1, Color.GRAY);
        Border rightBorder = BorderFactory.createMatteBorder(1, 1, 1, 4, Color.GRAY);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                ViewConstant.numberFields[i][j] = new JTextField();
                // 不可编辑
                ViewConstant.numberFields[i][j].setEditable(false);
                ViewConstant.numberFields[i][j].setHorizontalAlignment(JTextField.CENTER);
                ViewConstant.numberFields[i][j].setBackground(Color.WHITE);

                // 同时构建panel
                ViewConstant.numberPanels[i][j] = new JPanel();
                ViewConstant.numberPanels[i][j].setLayout(new GridLayout(3, 3, 0, 0));

                // 加边框！
                if (i == 2 && j == 2 || i == 2 && j == 5 || i == 5 && j == 2 || i == 5 && j == 5) {
                    ViewConstant.numberFields[i][j].setBorder(rightAndBottomBorder);
                    ViewConstant.numberPanels[i][j].setBorder(rightAndBottomBorder);
                } else if (j == 2 || j == 5) {
                    ViewConstant.numberFields[i][j].setBorder(rightBorder);
                    ViewConstant.numberPanels[i][j].setBorder(rightBorder);
                } else if (i == 2 || i == 5) {
                    ViewConstant.numberFields[i][j].setBorder(bottomBorder);
                    ViewConstant.numberPanels[i][j].setBorder(bottomBorder);
                } else {
                    ViewConstant.numberFields[i][j].setBorder(centerBorder);
                    ViewConstant.numberPanels[i][j].setBorder(centerBorder);
                }
                //对每个格子加上名称来标识
                ViewConstant.numberFields[i][j].setName(i * 9 + j + "");
                //对每个格子加上监听器
                ViewConstant.numberFields[i][j].addFocusListener(new SudoTextFocusListener());
                // 添加右键事件
//                ViewConstant.numberFields[i][j].addMouseListener(new SudoTextMouseListener());
                this.add(ViewConstant.numberFields[i][j]);

                // 添加标记面板
                ViewConstant.numberPanels[i][j].addMouseListener(new SudoTextMouseListener());
                for(int n = 0; n < 9; n++) {
                    ViewConstant.numberLabels[i][j][n] = new JTextField("");
                    ViewConstant.numberLabels[i][j][n].setName(i + "," + j + "," + n);
                    ViewConstant.numberLabels[i][j][n].setFont(new Font("方正仿宋简体", Font.BOLD, 14));
                    ViewConstant.numberLabels[i][j][n].setEditable(false);
                    ViewConstant.numberLabels[i][j][n].setHorizontalAlignment(JTextField.CENTER);
                    ViewConstant.numberLabels[i][j][n].setBorder(new EmptyBorder(0,0,0,0));
                    ViewConstant.numberLabels[i][j][n].setBackground(Color.WHITE);
                    // 获取焦点事件
                    ViewConstant.numberLabels[i][j][n].addFocusListener(new SudoTextFocusListener());
                    ViewConstant.numberPanels[i][j].add(ViewConstant.numberLabels[i][j][n]);
                }
            }
        }
    }

    /***
     * 功能描述: 渲染数独区域 <br/>
     *
     */
    public static void refreshSudoPanel() {
        SudoData.sudo.getBoxList().forEach(box -> {
            JTextField field = ViewConstant.numberFields[box.getX()][box.getY()];
            // 初始化的数值
            if(box.isInitNum()) {
                field.setFont(ViewConstant.initFont);
                field.setForeground(Color.BLACK);
                field.setText(box.getV() + "");
            } else {
                // 非初始化数值
                field.setFont(ViewConstant.zeroFont);
                field.setForeground(Color.BLUE);
                field.setText(box.getV() == 0 ? "" : box.getV() + "");
            }
            field.setBackground(Color.WHITE);
        });
    }

}
