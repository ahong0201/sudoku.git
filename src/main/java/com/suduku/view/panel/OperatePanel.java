package com.suduku.view.panel;

import com.suduku.view.constant.ViewConstant;
import com.suduku.view.data.RandSudoData;
import com.suduku.view.data.SudoData;
import com.suduku.view.dialog.InputDataDialog;
import com.suduku.view.listener.SudoClearMarkListener;
import com.suduku.view.listener.SudoHintListener;
import com.suduku.view.listener.SudoMarkAllListener;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * 功能描述: 操作区布局 <br/>
 */
public class OperatePanel extends JPanel {

    public OperatePanel() {
        this.setPreferredSize(new Dimension(150, 500));
        initPanel();
    }

    /**
     * 功能描述: 初始化页面 <br/>
     */
    public void initPanel() {
        ViewConstant.startBut.setFont(ViewConstant.butFont);
        ViewConstant.startBut.setBackground(ViewConstant.lightGray);
        ViewConstant.startBut.addActionListener(e -> {
            // 弹窗输入数据
            new InputDataDialog();
        });
        this.add(ViewConstant.startBut);

        // 添加提示按钮
        ViewConstant.hintBut.setFont(ViewConstant.butFont);
        ViewConstant.hintBut.setBackground(ViewConstant.lightGray);
        ViewConstant.hintBut.setEnabled(false);
        ViewConstant.hintBut.addActionListener(new SudoHintListener());

        this.add(ViewConstant.hintBut);

        // 添加 提示方法
        JPanel labelPanel = new JPanel();
        labelPanel.add(ViewConstant.calcLabel);
        this.add(labelPanel);

        // 难度选择
        JComboBox diffCom = new JComboBox();
        diffCom.addItem("      简单    ");
        diffCom.addItem("      中等    ");
        diffCom.addItem("      困难    ");
        diffCom.addItem("      专家    ");
        diffCom.setFont(ViewConstant.butFont);
        this.add(diffCom);

        // 随机开始
        ViewConstant.randStartBut.setFont(ViewConstant.butFont);
        ViewConstant.randStartBut.setBackground(ViewConstant.lightGray);
        ViewConstant.randStartBut.addActionListener(e -> {
            int grade;
            Random random = new Random();
            switch (diffCom.getSelectedIndex()) {
                case 0:
                    // [31, 40]
                    grade = random.nextInt(10) + 31;
                    break;
                case 1:
                    // [41, 45]
                    grade = random.nextInt(5) + 41;
                    break;
                case 2:
                    // [46, 50]
                    grade = random.nextInt(5) + 46;
                    break;
                case 3:
                    // [51, 55]
                    grade = random.nextInt(5) + 51;
                    break;
                default:
                    grade = 40;
                    break;
            }
            RandSudoData rsd = new RandSudoData();
            String str = rsd.randDataStr(grade);
            SudoData.initData(str);
            ViewConstant.markAllBut.setEnabled(true);
            ViewConstant.calcLabel.setText("                    ");
        });
        this.add(ViewConstant.randStartBut);

        // 全部标记按钮
        ViewConstant.markAllBut.setFont(ViewConstant.butFont);
        ViewConstant.markAllBut.setBackground(ViewConstant.lightGray);
        ViewConstant.markAllBut.setEnabled(false);
        ViewConstant.markAllBut.addActionListener(new SudoMarkAllListener());
        this.add(ViewConstant.markAllBut);

        // 全部标记按钮
        ViewConstant.clearMarkBut.setFont(ViewConstant.butFont);
        ViewConstant.clearMarkBut.setBackground(ViewConstant.lightGray);
        ViewConstant.clearMarkBut.setEnabled(false);
        ViewConstant.clearMarkBut.addActionListener(new SudoClearMarkListener());
        this.add(ViewConstant.clearMarkBut);

        // 添加标记按钮区域
        this.add(new MarkPanel());

    }

}
