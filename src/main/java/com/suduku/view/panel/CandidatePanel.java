package com.suduku.view.panel;

import com.suduku.view.constant.ViewConstant;
import com.suduku.view.listener.CandidateSetNumListener;

import javax.swing.*;
import java.awt.*;

/**
 * 功能描述: 候选区布局 <br/>
 */
public class CandidatePanel extends JPanel {

    public CandidatePanel() {
        this.setPreferredSize(new Dimension(600, 45));
        this.setLayout(new GridLayout(0, 11, 0, 0));
        //放选择按钮
        for (int i = 0; i < 11; i++) {
            ViewConstant.setNum[i] = new JButton();
            ViewConstant.setNum[i].setEnabled(false);
            if(i == 9) {
                ViewConstant.setNum[i].setText("<");
                ViewConstant.setNum[i].setToolTipText("回退");
                ViewConstant.setNum[i].setEnabled(true);
            } else  if (i == 10) {
                ViewConstant.setNum[i].setText("×");
                ViewConstant.setNum[i].setToolTipText("清除");
                //加清除的监听器
            } else {
                ViewConstant.setNum[i].setText(i + 1 + "");
                //加按钮的监听器
            }
            ViewConstant.setNum[i].setFont(ViewConstant.font);
            ViewConstant.setNum[i].setForeground(Color.WHITE);
            ViewConstant.setNum[i].setBackground(ViewConstant.green);
            ViewConstant.setNum[i].setPreferredSize(new Dimension(45, 35));
            ViewConstant.setNum[i].setFocusPainted(false);
            //加下方按钮监听器
            ViewConstant.setNum[i].addActionListener(new CandidateSetNumListener());
            this.add(ViewConstant.setNum[i]);
        }
    }

}
