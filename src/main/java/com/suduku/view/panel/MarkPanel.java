package com.suduku.view.panel;

import com.suduku.view.constant.ViewConstant;
import com.suduku.view.data.SudoData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 功能描述: 标记面板 <br/>
 */
public class MarkPanel extends JPanel implements ActionListener {

    private static final String fix =  "°";

    public MarkPanel() {
        this.setPreferredSize(new Dimension(150, 300));
        this.add(new JLabel("标记操作"), BorderLayout.NORTH);
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(150, 150));
        panel.setLayout(new GridLayout(3, 3, 0, 0));
        for(int i = 0; i < 9; i++) {
            ViewConstant.markBut[i] = new JButton(i + 1 + fix);
            ViewConstant.markBut[i].setFont(ViewConstant.markFont);
            ViewConstant.markBut[i].setForeground(Color.WHITE);
            ViewConstant.markBut[i].setBackground(ViewConstant.green);
            ViewConstant.markBut[i].addActionListener(this);
            ViewConstant.markBut[i].setEnabled(false);
            panel.add(ViewConstant.markBut[i]);
        }
        this.add(panel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = e.getActionCommand().substring(0, 1);
        ViewConstant.setMarkNum();
        // 获取标记的空间
        JTextField label = ViewConstant.numberLabels[SudoData.CX][SudoData.CY][Integer.parseInt(text) - 1];
        if("".equals(label.getText())) {
            label.setText(text);
        } else {
            label.setText("");
        }
    }

}
