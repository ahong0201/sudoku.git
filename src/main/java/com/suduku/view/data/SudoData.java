package com.suduku.view.data;

import com.suduku.calc.register.SudoCalcRegister;
import com.suduku.entity.Box;
import com.suduku.entity.Sudo;
import com.suduku.listener.impl.SudoPanelImpl;
import com.suduku.util.SudoUtil;
import com.suduku.view.constant.ViewConstant;

import java.util.HashMap;
import java.util.Map;

public class SudoData {

    /** 操作次数 */
    public static Integer count = 0;
    /** 存放历史操作数独 */
    public static Map<Integer, Sudo> hisSudo = new HashMap<>();
    /** 存放当前的数独 */
    public static Sudo sudo;
    /** 初始数独，用于保存当前解题的最初数据 */
    public static String initSudo;

    /** 当前选中的坐标 */
    public static Integer CX;
    /** 当前选中的坐标 */
    public static Integer CY;

    /** 有标记的坐标存放 */
    public static Map<String, Boolean> markMap = new HashMap<>();

    public static void initData(String str) {
        SudoUtil.isSudoCheck(str);
        initSudo = str;
        sudo = new Sudo(SudoUtil.toBoxList(str), new SudoPanelImpl());
        hisSudo.clear();
        count = 0;
        // 添加到历史中
        hisSudo.put(count, sudo);
        // 清理标记
        markMap.clear();
        ViewConstant.refreshSudoPanel();
        // 注册解析算法
        SudoCalcRegister.register(sudo);
    }

    public static void backup() {
        count++;
        Sudo clone = sudo.clone();
        hisSudo.put(count, clone);
    }

    public static void restore() {
        sudo = hisSudo.get(count);
        hisSudo.remove(count);
        count--;
        SudoCalcRegister.register(sudo);
    }

    public static Box getCurrentBox() {
        return SudoData.sudo.getBoxList().stream().filter(b -> b.getX() == CX && b.getY() == CY).findFirst().get();
    }

    public static Box getByIndex(String index) {
        if(SudoData.sudo != null) {
            return SudoData.sudo.getBoxList().stream().filter(i -> index.equals(i.getI() + "")).findFirst().get();
        }
        return new Box('0', -1);
    }

    public static Box getByIndex(Integer index) {
        if(SudoData.sudo != null) {
            return SudoData.sudo.getBoxList().stream().filter(i -> index == i.getI()).findFirst().get();
        }
        return new Box('0', -1);
    }

}
