package com.suduku.calc;

import com.suduku.entity.Box;
import com.suduku.util.SudoUtil;

import java.util.List;
import java.util.Map;

/**
 * 数链-垂直（数字n在某一列中，只有两个空白格B1，B2（不在同宫中）存在，
 * 并且在行中也只有两个空白格B3，B4（不在同宫中）。并且两个端点都在某个宫中。
 * 则另外两个端点相交的其他宫的空白格B5，则没有数字n） <br/>
 *
 * 测试数据：DataConstant.OTHER_SU_LIAN_V_01
 */
public class SuLianVerticalCalc extends AbstractCalc {

    /**
     *  1. 遍历所有数字
     *  2. 遍历所有列，获取只有两个候选值有数字n的空白格B1，B2，不能在同一宫中（可能存在多列）
     *  3. 遍历所有行，获取只有两个候选值有数字n的空白格B3，B4，不能在同一宫中（可能存在多行）
     *  4. 判断是否有两个顶点在同一个宫中。
     *  5. 如果存在有两个顶点在同一宫中，则另外两个顶点垂直相交的另一个点是空白格，则不包含数字n
     */
    @Override
    Box solve() {
        // 遍历数字
        for(Integer n : Box.INIT_LIST) {
            // 遍历所有列
            for (Map.Entry<Integer, List<Box>> yEntry : getYentries()) {
                // 获取B1,B2两个空白格
                List<Box> b12List = SudoUtil.findBoxByCList(yEntry.getValue(), n);
                if(SudoUtil.isTwoBox(b12List)) {
                    // 遍历所有行，获取B3,B4两个空白格
                    for (Map.Entry<Integer, List<Box>> xEntry : getXentries()) {
                        List<Box> b34List = SudoUtil.findBoxByCList(xEntry.getValue(), n);
                        if(SudoUtil.isTwoBox(b34List)) {
                            // B1B3相交同一宫中
                            Box clearBox = intersectAndClear(b12List.get(1), b12List.get(0), b34List.get(0), b34List.get(1), n);
                            if(clearBox != null) {
                                return clearBox;
                            }
                            // B1B4相交同一宫中
                            clearBox = intersectAndClear(b12List.get(1), b12List.get(0), b34List.get(1), b34List.get(0), n);
                            if(clearBox != null) {
                                return clearBox;
                            }
                            // B2B3相交同一宫中
                            clearBox = intersectAndClear(b12List.get(0), b12List.get(1), b34List.get(0), b34List.get(1), n);
                            if(clearBox != null) {
                                return clearBox;
                            }
                            // B2B4相交同一宫中
                            clearBox = intersectAndClear(b12List.get(0), b12List.get(1), b34List.get(1), b34List.get(0), n);
                            if(clearBox != null) {
                                return clearBox;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 功能描述: 相交与同一宫中，并且清理数字n <br/>
     * 
     * @param b1 列非相交点
     * @param b2 列相交点
     * @param b3 行相交点
     * @param b4 行非相交点
     * @param n 候选值n
     * @return "com.suduku.entity.Box"
     */
    private Box intersectAndClear(Box b1, Box b2, Box b3, Box b4, Integer n) {
        if(b2.getG() == b3.getG()) {
            Box box = getBoxByXY(b1.getX(), b4.getY());
            if(box != null && box.isBlank() && box.getCList().contains(n)) {
                box.removeCList(n);
                change(box, SudoUtil.getList(b1, b2, b3, b4), n);
                return box;
            }
        }
        return null;
    }

}
