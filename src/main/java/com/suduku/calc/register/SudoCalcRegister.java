package com.suduku.calc.register;

import com.suduku.calc.AbstractCalc;
import com.suduku.entity.Sudo;
import com.suduku.calc.enums.CalcEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 注册表，用于注册解题方法 <br/>
 *
 * @author chena
 */
public class SudoCalcRegister {

    /** 算法注册表 */
    public static Map<CalcEnum, AbstractCalc> CALC_MAP = new HashMap<>(CalcEnum.values().length);

    /**
     * 功能描述: 开始注册 <br/>
     *
     */
    public static void register(Sudo sudo) {
        for(CalcEnum ce : CalcEnum.values()) {
            sudo.getListener().sendMsg("开始注册：%s\n", ce.getName());
            AbstractCalc ac = AbstractCalc.getInstance(ce.getClazz());
            ac.setSudo(sudo);
            CALC_MAP.put(ce, ac);
        }
        sudo.getListener().sendMsg("算法注册完成！\n");
    }

    /**
     * 功能描述: 获取对应的算法 <br/>
     * 
     * @param ce 算法枚举
     * @return "com.suduku.calc.AbstractCalc"
     */
    public static AbstractCalc get(CalcEnum ce) {
        return CALC_MAP.get(ce);
    }

}
