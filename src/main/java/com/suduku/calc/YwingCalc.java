package com.suduku.calc;

import com.suduku.entity.Box;
import com.suduku.util.SudoUtil;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 与X-wing对应
 * 测试数据：SudoUtil.transpose(DataConstant.OTHER_X_WING_01) 转置后的数独
 */
public class YwingCalc extends AbstractCalc {

    @Override
    Box solve() {
        // 遍历数字
        for(Integer n : Box.INIT_LIST) {
            Set<Map.Entry<Integer, List<Box>>> entries = getXMap().entrySet();
            // 遍历区域
            for(Map.Entry<Integer, List<Box>> e1 : entries) {
                // 获取当前区域包含数字n的空白格
                List<Box> b1List = e1.getValue().stream().filter(b -> b.isBlank() && b.getCList().contains(n)).collect(Collectors.toList());
                // 如果只有两个空白格，并且不在同一宫中
                if(b1List.size() == 2 && b1List.get(0).getG() != b1List.get(1).getG()) {
                    // 遍历第二遍
                    for(Map.Entry<Integer, List<Box>> e2 : entries) {
                        if(e2.getKey() > e1.getKey()) {
                            // 获取当前区域包含数字n的空白格
                            List<Box> b2List = e2.getValue().stream().filter(b -> b.isBlank() && b.getCList().contains(n)).collect(Collectors.toList());
                            // 如果只有两个空白格，并且不在同一宫中
                            if(b2List.size() == 2 && b2List.get(0).getG() != b2List.get(1).getG()) {
                                // 判断是否在同一行中
                                if(b1List.get(0).getY() == b2List.get(0).getY() && b1List.get(1).getY() == b2List.get(1).getY()) {
                                    Box box = clearBoxNum(b1List.get(0), b2List.get(0), n);
                                    if(box != null) {
                                        change(box, SudoUtil.getList(b1List, b2List), n);
                                        return box;
                                    }
                                    box = clearBoxNum(b1List.get(1), b2List.get(1), n);
                                    if(box != null) {
                                        change(box, SudoUtil.getList(b1List, b2List), n);
                                        return box;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 功能描述: 清理数字 <br/>
     *
     * @param b1 同一行的单元格1
     * @param b2 同一行的单元格2
     * @param n 移除的数字
     * @return "com.suduku.entity.Box"
     */
    private Box clearBoxNum(Box b1, Box b2, Integer n) {
        List<Box> clearBoxList = getYList(b1).stream()
                .filter(b -> b.isBlank() && b.getI() != b1.getI() && b.getI() != b2.getI() && b.getCList().contains(n))
                .collect(Collectors.toList());
        for (Box box : clearBoxList) {
            getListener().sendMsg("移除第【%d】列的数字：%d\n", (b1.getX() + 1), n);
            box.removeCList(n);
            return box;
        }
        return null;
    }

}
