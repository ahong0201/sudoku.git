package com.suduku.calc;

import com.suduku.entity.Box;
import com.suduku.util.SudoUtil;

/**
 * 摒除法（数字在所在行或列或宫中，只有一个空格能够填写，则确定是唯一数字） <br/>
 * 测试数据：DataConstant.XING_01_50
 */
public class OnlyBoxCalc extends AbstractCalc {

    @Override
    Box solve() {
        // 遍历单元格列表
        long count;
        for(Box box : getBoxList()) {
            // 如果是空白格
            if(box.isBlank()) {
                // 遍历该单元格的候选值列表
                for(Integer n : box.getCList()) {
                    // 判断所在宫内是否唯一
                    count = SudoUtil.getNumCount(getGList(box), box.getI(), n);
                    if(count == 0) {
                        // 说明在该宫中，数字n只有当前单元格存在
                        box.setVAndClear(n);
                        change(box, getGList(box), n);
                        return box;
                    }
                    // 判断所在行内是否唯一
                    count = SudoUtil.getNumCount(getXList(box), box.getI(), n);
                    if(count == 0) {
                        // 说明在该行中，数字n只有当前单元格存在
                        box.setVAndClear(n);
                        change(box, getXList(box), n);
                        return box;
                    }
                    // 判断所在列内是否唯一
                    count = SudoUtil.getNumCount(getYList(box), box.getI(), n);
                    if(count == 0) {
                        // 说明在该宫中，数字n只有当前单元格存在
                        box.setVAndClear(n);
                        change(box, getYList(box), n);
                        return box;
                    }

                }
            }
        }
        return null;
    }

}
