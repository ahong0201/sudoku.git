package com.suduku.calc;

import com.suduku.entity.Box;
import com.suduku.util.SudoUtil;

/**
 * 唯一余数法（当前单元格中，候选数字只有一个） <br/>
 * 测试数据：DataConstant.RANDOM_01_01
 */
public class OnlyNumCalc extends AbstractCalc {

    @Override
    Box solve() {
        for(Box box : getBoxList()) {
            // 如果是空白格，并且候选数字只有一个，则确定为
            if(box.isBlank() && box.getCList().size() == 1) {
                box.setVAndClear(box.getCList().get(0));
                change(box, SudoUtil.getList(box), 0);
                return box;
            }
        }
        return null;
    }

}
