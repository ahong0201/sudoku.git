package com.suduku.calc.enums;

import com.suduku.calc.*;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述: 算法枚举 <br/>
 *
 */
@Getter
@AllArgsConstructor
public enum CalcEnum {

    /***/
    ONLY_NUM(OnlyNumCalc.class, "唯余法"),
    ONLY_BOX(OnlyBoxCalc.class, "摒除法"),
    GRID_XY(GridXYCalc.class, "单宫行列法"),
    SU_DUI(SuDuiCalc.class, "数对法"),
    X_WING(XwingCalc.class, "X-wing"),
    Y_WING(YwingCalc.class, "Y-wing"),
    XY_WING(XYwingCalc.class, "XY-wing"),
    YX_WING(YXwingCalc.class, "YX-wing"),
    XYZ_WING(XYZwingCalc.class, "XYZ-wing"),
    SU_LIAN_V(SuLianVerticalCalc.class, "数链垂直"),
    SU_LIAN_T(SuLianTrapezoidCalc.class, "数链梯形"),
    SU_LIAN_J(SuLianDingCalc.class, "数链多宝鱼"),
    SU_LIAN_K(SuLianKuaiCalc.class, "数链数块"),
    SU_LIAN_M(SuLianMoreCalc.class, "数链广义"),
    ;

    private static final Map<Class<? extends AbstractCalc>, CalcEnum> CE_MAP = new HashMap<>(CalcEnum.values().length);

    static {
        for(CalcEnum ce : CalcEnum.values()) {
            CE_MAP.put(ce.getClazz(), ce);
        }
    }

    /**
     * 功能描述: 通过类，获取枚举 <br/>
     *
     * @param clazz 类
     * @return "com.suduku.calc.enums.CalcEnum"
     */
    public static CalcEnum indexOf(Class<? extends AbstractCalc> clazz) {
        return CE_MAP.get(clazz);
    }

    private final Class<? extends AbstractCalc> clazz;
    private final String name;

}
