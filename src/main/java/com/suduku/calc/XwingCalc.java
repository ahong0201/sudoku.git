package com.suduku.calc;

import com.suduku.entity.Box;
import com.suduku.util.SudoUtil;

import java.util.*;
import java.util.stream.Collectors;

/**
 * X-wing（如果在同一列，并且不在同一宫中，存在数字n只有两个单元格可以填写；并且不在这两宫的其他列存在相同的情况，则可以排除在这两行中，其他位置的数字n的情况） <br/>
 * 与X-wing相对应的，横向也是满足的。
 * 将行和列合并起来，形成矩形。就能够排除4个顶点所在的行列的其余空白格的数字。
 * 测试数据：DataConstant.OTHER_X_WING_01
 */
public class XwingCalc extends AbstractCalc {

    @Override
    Box solve() {
        // 遍历数字
        for(Integer n : Box.INIT_LIST) {
            Set<Map.Entry<Integer, List<Box>>> entries = getYMap().entrySet();
            // 遍历区域
            for(Map.Entry<Integer, List<Box>> e1 : entries) {
                // 获取当前区域包含数字n的空白格
                List<Box> b1List = e1.getValue().stream().filter(b -> b.isBlank() && b.getCList().contains(n)).collect(Collectors.toList());
                // 如果只有两个空白格，并且不在同一宫中
                if(b1List.size() == 2 && b1List.get(0).getG() != b1List.get(1).getG()) {
                    // 遍历第二遍
                    for(Map.Entry<Integer, List<Box>> e2 : entries) {
                        if(e2.getKey() > e1.getKey()) {
                            // 获取当前区域包含数字n的空白格
                            List<Box> b2List = e2.getValue().stream().filter(b -> b.isBlank() && b.getCList().contains(n)).collect(Collectors.toList());
                            // 如果只有两个空白格，并且不在同一宫中
                            if(b2List.size() == 2 && b2List.get(0).getG() != b2List.get(1).getG()) {
                                // 判断是否在同一行中
                                if(b1List.get(0).getX() == b2List.get(0).getX() && b1List.get(1).getX() == b2List.get(1).getX()) {
                                    Box box = clearBoxNum(b1List.get(0), b2List.get(0), n);
                                    if(box != null) {
                                        change(box, SudoUtil.getList(b1List, b2List), n);
                                        return box;
                                    }
                                    box = clearBoxNum(b1List.get(1), b2List.get(1), n);
                                    if(box != null) {
                                        change(box, SudoUtil.getList(b1List, b2List), n);
                                        return box;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 功能描述: 清理数字 <br/>
     *
     * @param b1 同一列的单元格1
     * @param b2 同一列的单元格2
     * @param n 移除的数字
     * @return "com.suduku.entity.Box"
     */
    private Box clearBoxNum(Box b1, Box b2, Integer n) {
        List<Box> clearBoxList = getXList(b1).stream()
                .filter(b -> b.isBlank() && b.getI() != b1.getI() && b.getI() != b2.getI() && b.getCList().contains(n))
                .collect(Collectors.toList());
        for (Box box : clearBoxList) {
            box.removeCList(n);
            return box;
        }
        return null;
    }

}
