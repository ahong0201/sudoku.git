package com.suduku.listener.impl;

import com.suduku.calc.AbstractCalc;
import com.suduku.entity.Box;
import com.suduku.listener.SudoListener;
import com.suduku.util.SudoUtil;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 数独输出实现监听 <br/>
 *
 * @author chena
 */
public class SudoPrintImpl implements SudoListener {

    @Override
    public void sendMsg(String msg, Object ...args) {
        System.out.printf(msg, args);
    }

    @Override
    public void change(List<Box> list, Box b, List<Box> useBox) {
        sendMsg("确认位置【行：%d，列：%d】\t值为：【%s】\t候选值为：【%s】\n",
                b.getX() + 1, b.getY() + 1, b.getV() == 0 ? "" : b.getV(),
                b.getCList().stream().map(String::valueOf).collect(Collectors.joining(",")));
        SudoUtil.print(list, b, useBox);
    }

    @Override
    public void useCalc(AbstractCalc ac) {
        sendMsg("尝试【%s\t】\n", ac.getCalcEnum().getName());
    }

    @Override
    public void change(Box clearBox, List<Box> useBox, List<Box> boxList, Integer... ns) {
        for(Box box : useBox) {
            sendMsg(box.toString() + "\n");
        }
        sendMsg("清除单元格【%s】的候选值%s\n", clearBox.toString(), Arrays.asList(ns).toString());
        SudoUtil.print(boxList, clearBox, useBox);
    }

}
