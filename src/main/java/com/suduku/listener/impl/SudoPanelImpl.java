package com.suduku.listener.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.suduku.calc.AbstractCalc;
import com.suduku.entity.Box;
import com.suduku.listener.SudoListener;
import com.suduku.view.constant.ViewConstant;
import com.suduku.view.data.SudoData;

import java.awt.*;
import java.util.List;

public class SudoPanelImpl implements SudoListener {

    @Override
    public void sendMsg(String msg, Object... args) {
        System.out.printf(msg, args);
    }

    @Override
    public void change(List<Box> list, Box b, List<Box> useBox) {

    }

    @Override
    public void useCalc(AbstractCalc ac) {
//        new MessageDialog("通过【" + ac.getCalcEnum().getName() + "】");
        ViewConstant.calcLabel.setText("通过【" + ac.getCalcEnum().getName() + "】");
    }

    @Override
    public void change(Box clearBox, List<Box> useBox, List<Box> boxList, Integer... ns) {
        SudoData.CX = clearBox.getX();
        SudoData.CY = clearBox.getY();
        // 如果 clearBox 是确定的数字，则填写
        if(clearBox.isNumber()) {
            ViewConstant.setNum(clearBox.getV() + "");
        } else {
            // 如果 useBox中包含 clearBox，则清理 ns 之外的数字
            if(useBox.stream().anyMatch(b -> b.getI() == clearBox.getI())) {
                clearBox.getCList().forEach(c -> {
                    if (!CollUtil.contains(ListUtil.toList(ns), c)) {
                        ViewConstant.setMarkNum();
                        // 清除标记内容
                        ViewConstant.numberLabels[SudoData.CX][SudoData.CY][c - 1].setText("");
                    }
                });
            } else {
                for(Integer n : ns) {
                    ViewConstant.setMarkNum();
                    // 清除标记内容
                    ViewConstant.numberLabels[SudoData.CX][SudoData.CY][n - 1].setText("");
                }
            }
        }
        for(Box box : useBox) {
            if (box.isBlank()) {
                for (Integer n : ns) {
                    ViewConstant.numberLabels[box.getX()][box.getY()][n - 1].setBackground(Color.YELLOW);
                }
            }
            sendMsg(box.toString() + "\n");
        }

    }

}
