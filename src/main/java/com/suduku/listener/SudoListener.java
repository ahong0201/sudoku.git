package com.suduku.listener;

import com.suduku.calc.AbstractCalc;
import com.suduku.entity.Box;

import java.io.Serializable;
import java.util.List;

/**
 * 监听接口 <br/>
 *
 * @author chena
 */
public interface SudoListener extends Serializable {

    /**
     * 功能描述: 发送提示信息 <br/>
     * 
     * @param msg 消息内容
     */
    void sendMsg(String msg, Object ...args);

    /**
     * 功能描述: 改变内容 <br/>
     *
     * @param list 数独列表
     * @param b 单元格
     * @param useBox 使用确定位置的单元格
     */
    void change(List<Box> list, Box b, List<Box> useBox);

    /**
     * 功能描述: 使用算法 <br/>
     *
     * @param ac 算法
     */
    void useCalc(AbstractCalc ac);

    /**
     * 功能描述: 使用单元格确认 <br/>
     *
     * @param clearBox 被清理的单元格
     * @param useBox 多个单元格
     * @param boxList 多个单元格
     * @param ns 候选值
     */
    void change(Box clearBox, List<Box> useBox, List<Box> boxList, Integer... ns);

}
