package com.suduku.entity;

import cn.hutool.core.collection.CollUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * 单元格 <br/>
 *
 * @author chena
 */
@Data
public class Box implements Serializable {

    public static final List<Integer> INIT_LIST = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

    /** 值（0 ~ 9 ；其中 0 代表未填写） */
    private Integer v;
    /** 一维数组下标 */
    private int i;
    /** 二维数组行下标 */
    private int x;
    /** 二维数组列下标 */
    private int y;
    /** 所在宫 */
    private int g;
    /** 宫坐在行 */
    private int gx;
    /** 宫坐在列 */
    private int gy;
    /** 是否初始化数字 */
    private boolean initNum;
    /** 候选数字列表 */
    private List<Integer> cList;

    public Box(char c, int i) {
        this.v = c - 48;
        this.i = i;
        this.x = this.i / 9;
        this.y = this.i % 9;
        this.g = this.x / 3 + this.y / 3 + this.x / 3 * 2;
        this.gx = this.x / 3;
        this.gy = this.y / 3;
        this.initNum = this.v != 0;
//        this.cList = new ArrayList<>(this.v == 0 ? INIT_LIST : Collections.emptyList());
    }

    /**
     * 功能描述: 是否空白 <br/>
     * 0表示待填写
     * @return "boolean"
     */
    public boolean isBlank() {
        return v == 0;
    }

    /**
     * 功能描述: 是否数字 <br/>
     *
     * @return "boolean"
     */
    public boolean isNumber() {
        return v != 0;
    }

    /**
     * 功能描述: 设置值，并清理候选数字 <br/>
     *
     * @param v 确定的值
     */
    public void setVAndClear(Integer v) {
        if(v != 0) {
            this.v = v;
            this.cList.clear();
        }
    }
    
    /**
     * 功能描述: 候选值移除数字 <br/>
     * 
     * @param c 数字
     */
    public void removeCList(Integer c) {
        getCList().remove(c);
        if(getCList().size() == 1) {
            setVAndClear(getCList().get(0));
        }
    }

    /**
     * 功能描述: 候选值移除数字 <br/>
     *
     * @param cs 数字列表
     */
    public void removeCList(List<Integer> cs) {
        getCList().removeAll(cs);
        if(getCList().size() == 1) {
            setVAndClear(getCList().get(0));
        }
    }

    @Override
    public String toString() {
        return "Box{" +
                "v=" + v +
                ", i=" + i +
                ",\t x=" + x +
                ", y=" + y +
                ", cList=" + (CollUtil.isNotEmpty(cList) ? cList.toString() : "[]") +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Box box = (Box) o;
        return i == box.i;
    }

    @Override
    public int hashCode() {
        return i;
    }
}
