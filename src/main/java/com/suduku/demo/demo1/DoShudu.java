package com.suduku.demo.demo1;

import java.util.Random;

public class DoShudu {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub


        int[][] cells = newshudu();

        //cells=changeShu(cells,9);
        for (int k = 0; k < 9; k++) {
            for (int i = 0; i < 9; i++) {
                System.out.print(cells[k][i]);
            }
            System.out.println();
        }
    }


    public static int[][] newshudu() {

        int[][] cells = new int[][]{

                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {4, 5, 6, 7, 8, 9, 1, 2, 3},
                {7, 8, 9, 1, 2, 3, 4, 5, 6},
                {2, 3, 1, 5, 6, 4, 8, 9, 7},
                {5, 6, 4, 8, 9, 7, 2, 3, 1},
                {8, 9, 7, 2, 3, 1, 5, 6, 4},
                {3, 1, 2, 6, 4, 5, 9, 7, 8},
                {6, 4, 5, 9, 7, 8, 3, 1, 2},
                {9, 7, 8, 3, 1, 2, 6, 4, 5}

        };


        int countH = new Random().nextInt(10);
        for (int k = 0; k < countH; k++) {
            cells = lineTolie(cells);

        }

        int count = 0;
        for (int k = 0; k < 12; k++) {
            count = new Random().nextInt(9);
            cells = changeLine(cells, count);

        }

        int countH2 = new Random().nextInt(10);
        for (int k = 0; k < countH2; k++) {
            cells = lineTolie(cells);

        }
        return cells;
    }

    public static int[][] changeLine(int[][] cells, int m) {//行与行交换
        int n = m;
        int[] temp = new int[9];
        n = ((m + 3) >= 9) ? (m + 3 - 9) : m + 3;
        for (int j = 0; j < 9; j++) {
            temp[j] = cells[m][j];
            cells[m][j] = cells[n][j];
            cells[n][j] = temp[j];

        }
        return cells;

    }


    public static int[][] lineTolie(int[][] cells) {//行与列互换

        int temp = 0;
        for (int j = 0; j < 9; j++) {
            for (int k = j + 1; k < 9; k++) {
                temp = cells[k][j];
                cells[k][j] = cells[j][k];
                cells[j][k] = temp;

            }
        }
        return cells;


    }

} 
