package com.suduku;

import com.suduku.constant.DataConstant;
import com.suduku.handle.SudoHandler;

/**
 * 主入口 <br/>
 *
 * @author chena
 */
public class SudoMain {

    public static void main(String[] args) {
        SudoHandler sudoHandler = new SudoHandler(DataConstant.OTHER_SU_LIAN_G_01);
        sudoHandler.calculate();
    }

}
