package com.robot;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import lombok.SneakyThrows;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class AutoClickRobot {
    private JTextField timeText;
    private JButton startBut;
    private JButton pauseBut;
    private JButton endBut;
    private JPanel jp1;
    private JPanel jp4;
    private JPanel jp2;
    private JPanel jp3;
    private JLabel timeLab;
    private JCheckBox pressCheckBox;
    private JButton recordBut;

    private boolean pauseFlag = false;
    private boolean runFlag = false;
    private boolean recordFlag = false;

    private final List<Point> pointList = new ArrayList<>();

    public AutoClickRobot() {
        startBut.addActionListener(e -> {
            System.out.println("点击了开始按钮");
            runFlag = true;
            // 3秒后开始执行
            new Timer().schedule(new RunClickTask(), 3000);
            startBut.setEnabled(false);
            pauseBut.setEnabled(true);
            endBut.setEnabled(true);
            recordBut.setEnabled(false);
        });
        pauseBut.addActionListener(e -> {
            System.out.println("点击了暂停按钮");
            pauseFlag = !pauseFlag;
            if (pauseFlag) {
                pauseBut.setText("继续");
            } else {
                pauseBut.setText("暂停");
            }
        });
        endBut.addActionListener(e -> {
            System.out.println("点击了结束按钮");
            runFlag = false;
            pauseFlag = false;
            recordFlag = false;
            pauseBut.setText("暂停");
            startBut.setEnabled(true);
            pauseBut.setEnabled(false);
            endBut.setEnabled(false);
            recordBut.setEnabled(true);
            pressCheckBox.setSelected(false);
            pointList.clear();
        });
        RecordKeyAdapter recordKeyAdapter = new RecordKeyAdapter();
        recordBut.addActionListener(e -> {
            recordFlag = !recordFlag;
            if (recordFlag) {
                // 正在录制鼠标位置
                recordBut.setText("结束录制");
                startBut.setEnabled(false);
                recordBut.addKeyListener(recordKeyAdapter);
            } else {
                // 录制结束
                recordBut.setText("开始录制");
                startBut.setEnabled(true);
                recordBut.removeKeyListener(recordKeyAdapter);
            }
        });
    }

    /**
     * 按键适配器 <br/>
     */
    class RecordKeyAdapter extends KeyAdapter {
        @Override
        public void keyReleased(KeyEvent e) {
            // 按键抬起时
            if (e.getKeyCode() == KeyEvent.VK_P) {
                System.out.println("点击了按键P，确定录制该鼠标位置。");
                Point point = MouseInfo.getPointerInfo().getLocation();
                pointList.add(point);
            }
        }
    }

    /**
     * 功能描述: 运行点击线程 <br/>
     */
    class RunClickTask extends TimerTask {

        @SneakyThrows
        @Override
        public void run() {
            // 创建机器人
            Robot robot = new Robot();
            while (runFlag) {
                if (pauseFlag) {
                    System.out.println("正在暂停。。。");
                    Thread.sleep(1000);
                } else {
                    if (pointList.size() == 0) {
                        // 获取鼠标当前位置
                        Point point = MouseInfo.getPointerInfo().getLocation();
                        mouseClick(robot, point, false);
                    } else {
                        // 执行录制的区域
                        for (Point point : pointList) {
                            mouseClick(robot, point, true);
                        }
                    }
                }
            }
        }
    }

    /**
     * 功能描述: 模拟鼠标点击 <br/>
     *
     * @param robot 机器人
     * @param point 鼠标位置
     */
    private void mouseClick(Robot robot, Point point, boolean moveOriPoint) throws InterruptedException {
        if (moveOriPoint) {
            robot.mouseMove(-1, -1);
            // 移动鼠标到指定位置
            robot.mouseMove((int) (point.getX() / 1.25), (int) (point.getY() / 1.25));
        } else {
            // 移动鼠标到指定位置
            robot.mouseMove((int) point.getX(), (int) point.getY());
        }
        if (pressCheckBox.isSelected()) {
            // 模拟鼠标左键按下
            robot.mousePress(InputEvent.BUTTON1_MASK);
        } else {
            System.out.println("x=" + point.getX() + ",y=" + point.getY());
            // 模拟鼠标左键按下
            robot.mousePress(InputEvent.BUTTON1_MASK);
            // 模拟鼠标左键松开
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
        }
        Thread.sleep(Integer.parseInt(timeText.getText()));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("TestGui");
        frame.setContentPane(new AutoClickRobot().jp1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        jp1 = new JPanel();
        jp1.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        jp2 = new JPanel();
        jp2.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        jp1.add(jp2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        jp3 = new JPanel();
        jp3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        jp2.add(jp3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        timeLab = new JLabel();
        timeLab.setText("间隔时间：");
        jp3.add(timeLab, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        timeText = new JTextField();
        timeText.setText("300");
        jp2.add(timeText, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        pressCheckBox = new JCheckBox();
        pressCheckBox.setText("长按");
        jp2.add(pressCheckBox, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        jp4 = new JPanel();
        jp4.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
        jp1.add(jp4, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        startBut = new JButton();
        startBut.setText("开始");
        jp4.add(startBut, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        pauseBut = new JButton();
        pauseBut.setEnabled(false);
        pauseBut.setText("暂停");
        jp4.add(pauseBut, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        endBut = new JButton();
        endBut.setEnabled(false);
        endBut.setText("结束");
        jp4.add(endBut, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        recordBut = new JButton();
        recordBut.setLabel("开始录制");
        recordBut.setText("开始录制");
        jp4.add(recordBut, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return jp1;
    }

}
