package com.rate;

import java.math.BigDecimal;

/**
 * 功能描述: 民间利息计算 <br/>
 *
 */
public class FolkRateCalc extends BaseRateCalc {


    public FolkRateCalc(RateInfoEntity rateInfoEntity) {
        super(rateInfoEntity);
    }

    /**
     * 功能描述: TODO <br/>
     * 利息按照全部本金算，每个月需要还本和利息
     *
     */
    @Override
    public void cacl() {
        // 计算每个月利息 = 本金 * 利率
        BigDecimal interest = rateInfoEntity.getPrincipal().multiply(rateInfoEntity.getRateMonth());
        System.out.println("每月利息为：" + interest);
        BigDecimal principalMonth = rateInfoEntity.getPrincipal().divide(rateInfoEntity.getDurationMonth(), 2, BigDecimal.ROUND_HALF_UP);
        BigDecimal totalInterest = BigDecimal.ZERO;
        Integer month = Integer.valueOf(rateInfoEntity.getDurationMonth().toString());
        // 还款计划
        for(int i = 1; i <= month; i++) {
            System.out.println("第" + i + "个月，归还利息：" + interest + "\t归还本金" + principalMonth + "\t共" + interest.add(principalMonth));
            totalInterest = totalInterest.add(interest);
        }
        System.out.println("借款" + rateInfoEntity.getPrincipal() + "元，利率为：" + rateInfoEntity.getRateMonth() + "每月，借款" + rateInfoEntity.getDurationMonth() + "月，总利息为：" + totalInterest);
    }
}
