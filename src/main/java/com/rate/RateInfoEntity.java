package com.rate;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 功能描述: 利率信息 <br/>
 *
 */
@Data
@ToString
public class RateInfoEntity {

    /** 本金 */
    private BigDecimal principal;

    /** 年利率 */
    private BigDecimal rateYear;

    /** 月利率 */
    private BigDecimal rateMonth;

    /** 日利率 */
    private BigDecimal rateDay;

    /** 年限 */
    private BigDecimal durationYear;

    /** 月限 */
    private BigDecimal durationMonth;

    /** 天 */
    private BigDecimal durationDay;

//    private

    // 方式。。。

    public static RateInfoEntity createYear(String principal, String rate, Integer duration) {
        RateInfoEntity re = new RateInfoEntity();
        re.setPrincipal(new BigDecimal(principal));
        re.setRateYear(new BigDecimal(rate));
        re.setDurationYear(new BigDecimal(duration));
        // 转换成月
        re.setRateMonth(re.getRateYear().divide(new BigDecimal(12), 10, BigDecimal.ROUND_HALF_UP));
        re.setDurationMonth(re.getDurationYear().multiply(new BigDecimal(12)));
        // 转换成日
        re.setRateDay(re.getRateYear().divide(new BigDecimal(360), 10, BigDecimal.ROUND_HALF_UP));
        re.setDurationDay(re.getDurationYear().multiply(new BigDecimal(360)));
        return re;
    }

    public static RateInfoEntity createNonth(String principal, String rate, Integer duration) {
        RateInfoEntity re = new RateInfoEntity();
        re.setPrincipal(new BigDecimal(principal));
        re.setRateMonth(new BigDecimal(rate));
        re.setDurationMonth(new BigDecimal(duration));
        // 转换成年
        re.setRateYear(re.getRateMonth().multiply(new BigDecimal(12)));
        re.setDurationYear(re.getDurationMonth().divide(new BigDecimal(12), 10, BigDecimal.ROUND_HALF_UP));
        // 转换成日
        re.setRateDay(re.getRateMonth().divide(new BigDecimal(30), 10, BigDecimal.ROUND_HALF_UP));
        re.setDurationDay(re.getDurationMonth().multiply(new BigDecimal(30)));
        return re;
    }

    public static RateInfoEntity createDay(String principal, String rate, Integer duration) {
        RateInfoEntity re = new RateInfoEntity();
        re.setPrincipal(new BigDecimal(principal));
        re.setRateDay(new BigDecimal(rate));
        re.setDurationDay(new BigDecimal(duration));
        // 转换成年
        re.setRateYear(re.getRateDay().multiply(new BigDecimal(360)));
        re.setDurationYear(re.getDurationDay().divide(new BigDecimal(360), 10, BigDecimal.ROUND_HALF_UP));
        // 转换成月
        re.setRateMonth(re.getRateMonth().multiply(new BigDecimal(30)));
        re.setDurationMonth(re.getDurationMonth().divide(new BigDecimal(30), 10, BigDecimal.ROUND_HALF_UP));
        return re;
    }

}

