package com.rate;

import java.math.BigDecimal;

public class IntBeforeBenefits extends BaseRateCalc {

    public IntBeforeBenefits(RateInfoEntity rateInfoEntity) {
        super(rateInfoEntity);
    }

    /**
     * 功能描述: 先息后本 <br/>
     *
     */
    @Override
    public void cacl() {
        Integer month = Integer.valueOf(rateInfoEntity.getDurationMonth().toString());
        // 每月利息为
        BigDecimal interest = rateInfoEntity.getPrincipal().multiply(rateInfoEntity.getRateMonth());
        BigDecimal totalInterest = BigDecimal.ZERO;
        for(int i = 1; i <= month; i++) {
            System.out.println("每月利息为：" + interest);
            totalInterest = totalInterest.add(interest);
        }
        System.out.println("总利息为：" + totalInterest);
    }
}
