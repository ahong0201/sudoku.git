package com.rate;

/**
 * 功能描述: 利率计算基类 <br/>
 *
 */
public abstract class BaseRateCalc {

    protected RateInfoEntity rateInfoEntity;

    public BaseRateCalc(RateInfoEntity rateInfoEntity) {
        this.rateInfoEntity = rateInfoEntity;
    }

    public abstract void cacl();

}
